# Huge Navigation Exercise

## Overview

This exercise will have the candidate build a responsive site navigation driven by an AJAX request.

Here are the guidelines for this exercise

* No frameworks or libraries (e.g. jQuery, Angular, React).
* Chrome compliance is all that's required, all functions and features available in Chrome are in play.
* Nav must be responsive.
* Code must run after the following command, please ensure your code runs as you expect it to from a fresh checkout with these commands before submission.

```
$ npm i && grunt && npm start
```

## Version
0.0.1

###Install the original base exercise locally
```
git clone git@github.com:hugeinc/NavExercise.git
cd NavExercise
npm install
npm start
```

###Install the base exercise locally
```
git clone git@bitbucket.org:anderson1564/huge-nav-excercise.git
cd NavExercise
npm install
grunt
npm start
```

###Optional grunt parameters
* grunt - Execute the default grunt task
* grunt watch - Enable livereload
* grunt dev - Like grunt by with jshint validations

###About this code
This code use the following tools
* Grunt
* Stylus
* Jshint
* Node js

