/**
 * Created by anderson on 12/24/15.
 */

var createUl, createLi;

/**
 * Remove class name
 *
 * @param element
 * @param className
 */
function removeClass(element, className) {
    'use strict';
    element.classList.remove(className);
}

/**
 * Add class name
 *
 * @param element
 * @param className
 */
function addClass(element, className) {
    'use strict';
    element.classList.add(className);
}

/**
 * Check if element has a class
 *
 * @param element
 * @param expectedClassName
 * @returns {boolean}
 */
function hasClass(element, expectedClassName) {
    'use strict';
    var classList = element.classList;
    var result = false;
    for(var i = 0; i<classList.length; ++i) {
        var tempClass = classList[i];
        if(tempClass === expectedClassName) {
            result = true;
            break;
        }
    }
    return result;
}

/**
 * Check if the element is hidden
 *
 * @param element
 * @returns {boolean}
 */
function isHidden(element) {
    'use strict';
    return hasClass(element, 'hidden');
}

/**
 * Check if the element is hidden in mobile
 * @param element
 * @returns {boolean}
 */
function isHiddenMobile(element) {
    'use strict';
    return hasClass(element, 'hidden-mobile');
}

/**
 * Find child by class
 *
 * @param element
 * @param nameClass
 * @returns {*}
 */
function findChildByClass(element, nameClass) {
    'use strict';
    var childs = element.childNodes;
    var requiredChild = null;
    for (var i = 0; i < childs.length; ++i) {
        var child = childs[i];
        if (hasClass(child, nameClass)) {
            requiredChild = child;
            break;
        }
    }
    return requiredChild;
}

/**
 * Make visible a element
 *
 * @param element
 */
function showElement(element) {
    'use strict';
    removeClass(element, 'hidden');
    removeClass(element, 'hidden-mobile');
}

/**
 * Make invisible a element
 *
 * @param element
 */
function hideElement(element) {
    'use strict';
    if(!isHidden(element)) {
        addClass(element, 'hidden');
    }
}

/**
 * Make invisible a element in mobile
 *
 * @param element
 */
function hideElementMobile(element) {
    'use strict';
    if(!isHiddenMobile(element)) {
        addClass(element, 'hidden-mobile');
    }
}

/**
 * Display translucent mask
 */
function showTranslucentMask() {
    'use strict';
    var translucentMask = document.getElementById('translucent-mask');
    showElement(translucentMask);
}

/**
 * Hide translucent mask
 */
function hideTranslucentMask() {
    'use strict';
    var translucentMask = document.getElementById('translucent-mask');
    hideElement(translucentMask);
}

/**
 * Hide translucent mask
 */
function hideTranslucentMaskMobile() {
    'use strict';
    var translucentMask = document.getElementById('translucent-mask');
    hideElementMobile(translucentMask);
}

/**
 * Invert arrow
 *
 * @param element
 */
function invertArrow(element) {
    'use strict';
    var submenu = findChildByClass(element, 'submenu');
    if(!hasClass(submenu, 'invert')) {
        addClass(submenu, 'invert');
    }
}

/**
 * Restore arrows to the orignal state
 */
function restoreArrows() {
    'use strict';
    var submenus = document.getElementsByClassName('submenu');
    for(var i=0; i<submenus.length; ++i) {
        var submenu = submenus[i];
        removeClass(submenu, 'invert');
    }
}

/**
 * Close all sub menus
 */
function closeSubMenus() {
    'use strict';
    // get all sub menus
    var navItems = document.getElementsByClassName('nav-item');
    var navItemsLength = navItems.length;
    // Hide all sub menus
    for(var i =0; i<navItemsLength; ++i) {
        var navItem = navItems[i];
        hideElement(navItem);
    }
    restoreArrows();
    hideTranslucentMask();
}

/**
 * Open a sub menu
 * @param event
 */
function openSubMenu(event) {
    'use strict';
    var element = event.currentTarget;
    var navItem = findChildByClass(element, 'nav-item');
    // Display sub menu
    if(navItem !== null) {
        closeSubMenus();
        showElement(navItem);
        invertArrow(element);
        showTranslucentMask();
    }
}

/**
 * Insert text in a DOM element
 *
 * @param element
 * @param text
 */
function addTextElement(element, text) {
    'use strict';
    // Create a text node
    var textElement = document.createTextNode(text);
    // Append the text to element
    element.appendChild(textElement);
}

/**
 * Create a DOM element <div>
 *
 * @returns {Element}
 */
function createDiv() {
    'use strict';
    var div = document.createElement('div');
    return div;
}

/**
 * Create a DOM element <a>
 *
 * @param label
 * @param url
 * @returns {Element}
 */
function createA(label, url) {
    'use strict';
    var a = document.createElement('a');
    a.setAttribute('href', url);
    addTextElement(a, label);
    return a;
}

/**
 * Crate item menu
 *
 * @param label
 * @param url
 * @returns {Element}
 */
function createItemMenu(label, url) {
    'use strict';
    var a = createA(label, url);
    var div = createDiv();
    div.appendChild(a);
    return div;
}

/**
 *  Create down arrow
 *
 * @returns {Element}
 */
function createDownArrow() {
    'use strict';
    var downArrow = document.createElement('i');
    downArrow.setAttribute('class', 'fa fa-angle-down');
    return downArrow;
}

/**
 * Append down arrow to DOM element
 *
 * @param itemMenu
 */
function appendDownArrow(itemMenu) {
    'use strict';
    var downArrow = createDownArrow();
    itemMenu.appendChild(downArrow);
}

/**
 * Create a DOM element <li>
 *
 * @param label
 * @param url
 * @returns {Element}
 */
createLi = function (item) {
    'use strict';
    var label = item.label;
    var url = item.url;
    var subitems = item.items;

    var li = document.createElement('LI');
    var itemMenu = createItemMenu(label, url); // Create item menu
    li.appendChild(itemMenu);

    var ul = createUl(subitems); // Create sub menu
    if(ul !== null) {
        li.appendChild(ul);
        li.addEventListener('click', openSubMenu);
        ul.setAttribute('class', 'nav-item hidden');
        itemMenu.setAttribute('class', 'submenu');
        appendDownArrow(itemMenu);
    }
    return li;
};

/**
 * Create a DOM element <ul>
 *
 * @param items
 * @returns {*}
 */
createUl = function (items) {
    'use strict';
    if(typeof (items) !== 'undefined') {
        var itemsLength = items.length;
        if (itemsLength > 0) {
            var ul = document.createElement('UL');
            // Create menu items
            for (var i = 0; i < itemsLength; ++i) {
                var item = items[i];
                var li = createLi(item); // Create menu item
                ul.appendChild(li);
            }
            return ul;
        }
    }
    return null;
};

/**
 * Open sub menus mobile
 */
function openSubMenusMobile() {
    'use strict';
    var menuList = document.getElementById('menu-list');
    var menuMobile = document.getElementById('menu-mobile');
    showElement(menuList);
    hideElement(menuMobile);
    showTranslucentMask();
}

/**
 * Close sub menus mobile
 */
function closeSubMenusMobile() {
    'use strict';
    var menuList = document.getElementById('menu-list');
    var menuMobile = document.getElementById('menu-mobile');
    hideElementMobile(menuList);
    showElement(menuMobile);
    hideTranslucentMaskMobile();
}

/**
 * Init the nav
 *
 * @param items
 */
function initNav(items) {
    'use strict';
    var itemsLength = items.length;
    var list = document.getElementById('nav-list'); // Get the <ul> element to insert a new node

    // Create menu items
    for(var i=0; i < itemsLength; ++i) {
        var item = items[i];
        var li = createLi(item); // Create menu item
        list.appendChild(li);
    }
    // Attach event to close the sub menus
    var translucentMask = document.getElementById('translucent-mask');
    translucentMask.addEventListener('click', closeSubMenus);
    translucentMask.addEventListener('click', closeSubMenusMobile);

    // Attach event to open mobile sub menus
    var hamburger = document.getElementById('hamburguer');
    hamburger.addEventListener('click', openSubMenusMobile);

    // Attach event to close mobile sub menus
    var close = document.getElementById('close');
    close.addEventListener('click', closeSubMenusMobile);
}

/**
 * Request the data to load the nav
 *
 * @param resource
 */
function loadNav(resource) {
    'use strict';
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState === 4 && xhttp.status === 200) {
            var response = xhttp.responseText, // Get the response in plain text
                json = JSON.parse(response); // Convert the response on json

            // Init the nav if is possible
            if (typeof(json.items) !== 'undefined' && json.items.length > 0) {
                initNav(json.items);
            }
            else {
                // Display a error message
                document.getElementById('error').innerHTML = 'Error Loading The Page';
            }
        }
    };
    xhttp.open('GET', resource, true);
    xhttp.send();
}

// Init
(function(){
    'use strict';
    var resource = '/api/nav.json';
    loadNav(resource);
})();