module.exports = function(grunt) {

    // Project configuration.
    'use strict';
    grunt.initConfig({
        jshint: {
            options: grunt.file.readJSON('.jshintrc'),
            all: ['Gruntfile.js', 'src/js/*.js'],
        },
        uglify: {
            scripts: {
                files: {
                    // compile and concat into single file
                    'public/js/main.min.js': ['src/js/*.js']
                }
            }
        },
        stylus: {
            compile: {
                options: {
                    linenos: false,
                    paths: ['src/components'],
                    relativeDest: 'public/styles',
                    urlfunc: 'embedurl',
                    'resolve url': true
                },
                files: {
                    // Compile and concat into single file
                    'main.min.css': ['src/styl/main.styl']
                    // Compile and concat all styl files
                    //'main.min.css': ['src/styl/*.styl']
                }
            }
        },
        htmlmin: {
            dist: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: [{
                    expand: true,
                    cwd: 'src/html',
                    src: '**/*.html',
                    dest: 'public/'
                }]
            }
        },
        imagemin: {
            dist: {
                options: {
                    optimizationLevel: 5
                },
                files: [{
                    expand: true,
                    cwd: 'src/images',
                    src: ['**/*.{png,jpg,gif,svg}'],
                    dest: 'public/images'
                }]
            }
        },
        watch: {
            gruntfile: {
                files: 'Gruntfile.js',
                tasks: ['default'],
            },
            scripts: {
                files: ['src/js/*.js'],
                tasks: ['uglify'],
                options: {
                    livereload: true,
                    spawn: false,
                }
            },
            css: {
                files: ['src/styl/*.styl'],
                tasks: ['stylus'],
                options: {
                    livereload: true,
                    spawn: false,
                }
            },
            html: {
                files: ['src/html/*.html'],
                tasks: ['htmlmin'],
                options: {
                    livereload: true,
                    spawn: false,
                },
            },
            images: {
                files: ['src/images/*.{png,jpg,gif,svg}'],
                tasks: ['htmlmin'],
                options: {
                    livereload: true,
                    spawn: false,
                },
            },
        },
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    // Load the plugin that provides the "stylus" task.
    grunt.loadNpmTasks('grunt-contrib-stylus');
    // Load the plugin that provides the "watch" task
    grunt.loadNpmTasks('grunt-contrib-watch');
    // Load the plugin that provides the 'jshint' task
    grunt.loadNpmTasks('grunt-contrib-jshint');
    // Load the plugin that provides the "htmlmin" task
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    // Load the plugin that provides the "imagemin" task
    grunt.loadNpmTasks('grunt-contrib-imagemin');

    // Default task(s).
    grunt.registerTask('default', ['htmlmin', 'uglify', 'stylus', 'imagemin']);
    grunt.registerTask('dev', ['htmlmin', 'uglify', 'stylus', 'imagemin', 'jshint']);
};